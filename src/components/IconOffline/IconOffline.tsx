import React, {FunctionComponent, useEffect, useState} from "react";
import img from "../../img/unavailable-cloud.png";
import './IconOffline.css'

export const IconOffline: FunctionComponent = () => {
    const [isOnline, setIsOnline] = useState(true)

    useEffect(() => {
        function goOffline() {
            if (!navigator.onLine) {
                setIsOnline(false)
            } else setIsOnline(true)
        }
        window.addEventListener("offline", goOffline);
        window.addEventListener("online", goOffline);
        return function cleanup() {
            window.removeEventListener("offline", goOffline);
            window.removeEventListener("online", goOffline);
        }
    },[isOnline])

    const isShowClass = isOnline ? 'hide' : 'show'

    return (
       <img className={['offline-icon', isShowClass].join(' ')}
           width='64'
           src={img}
           alt='offline icon'/>
    )
}